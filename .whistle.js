const pkg = require('./package.json');

exports.name = `uniapp-composition-api-ts-base开发配置`;
exports.rules = `

# 开发环境
# test.com x.x.x.x # 可以直接转发到该ip

# 本地开发
^test.com/*** 127.0.0.1:7777
#  test.com/getExample 127.0.0.1:7777
#  $test.com/** 127.0.0.1:7777
#  https://test.com 127.0.0.1:7777
# 不走域名本地开发
#  ^localhost:8080/app/*** test.com/$1
`;
