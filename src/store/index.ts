import Vue from 'vue';
import Vuex from 'vuex';
// import { local } from './local/index';
// import User from './modules/example-no-decorator-module';

Vue.use(Vuex);
const store = new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    // User 非装饰器版本需要导入
  },
  plugins: [
    // local 需要使用去掉注释即可
  ]
});

export default store;
