/**
 * 非装饰器版本
 */
import { Module } from 'vuex';

interface ExampleModuleStateType {
  name: string;
}

const example: Module<ExampleModuleStateType, {}> = {
  state: {
    name: 'test'
  },
  mutations: {
    CHANGE_NAME(state, payload: string) {
      state.name = payload;
    }
  }
};
export default example;
