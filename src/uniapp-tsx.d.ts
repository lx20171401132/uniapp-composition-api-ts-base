import { VNode } from 'vue';
import { ComponentRenderProxy } from '@vue/composition-api';

declare global {
  namespace JSX {
    // tslint:disable no-empty-interface
    type Element = VNode;
    type ElementClass = ComponentRenderProxy;
    // tslint:disable no-empty-interface
    interface IntrinsicElements {
      [elem: string]: any;
    }
    interface ElementAttributesProperty {
      $props: any; // specify the property name to use
    }
  }
}
