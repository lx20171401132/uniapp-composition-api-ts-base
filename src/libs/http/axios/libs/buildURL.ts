const buildURL = require('axios/lib/helpers/buildURL');

/**
 * Build a URL by appending params to the end
 *
 * @param {string} url The base of the url (e.g., http://www.google.com)
 * @param {object} [params] The params to be appended
 * @returns {string} The formatted url
 */
function buildURLTool(url: string | undefined, params: object, paramsSerializer: Function | undefined) {
  return buildURL(url, params, paramsSerializer);
}

export default buildURLTool;
