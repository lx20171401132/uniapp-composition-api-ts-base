import Axios from 'axios';
import fitUniapp from './fit-uniapp';
import { setRequestConfig, setRequestError } from './request-interceptor';
import { setResponseConfig, setResponseError } from './response-interceptor';
import { HTTP_CONFIG } from '@/config';
export * from './fit-uniapp';

// 使用自定义适配器，适配uniapp
fitUniapp(Axios);

// 配置默认接口
const Instance = Axios.create({
  baseURL: HTTP_CONFIG.API_HOST,
  timeout: 7000
});

// 请求拦截
Instance.interceptors.request.use(setRequestConfig, setRequestError);

// 响应拦截
Instance.interceptors.response.use(setResponseConfig, setResponseError);

export default Instance;
