import store from '@/store/index';
// 导入自定义的弹窗
import { Message } from '@/libs/index';
/**
 * token 无效/过期，初始化状态，跳转登陆
 */
export const clearLogin = async () => {
  await store.dispatch('logOut');
  // 跳转登陆
  uni.reLaunch({
    url: '/pages/Login/index'
  });
};

/**
 * token为空，初始化状态，跳转登陆页面
 */
export const notLogin = () => {
  uni.showModal({
    title: '请登录',
    content: '您未登录，是否前往登陆？',
    async success(res) {
      if (res.confirm) {
        console.log('用户点击确定');
        await store.dispatch('logOut');

        // uni.reLaunch({
        //   url: '/pages/Login/index',
        // });
        // 跳转登陆
        uni.navigateTo({
          url: '/pages/Login/index'
        });
      } else if (res.cancel) {
        console.log('用户点击取消');
        Message.warning('请登陆哦！');
      }
    }
  });
};
