import { clearLogin, notLogin } from './index';

/**
 * 业务状态码
 */
export enum RESULT_CODE_KEY_TYPE {
  DEFAULT = 'DEFAULT'
}

const RESULT_CODE: Record<RESULT_CODE_KEY_TYPE, string> = {
  [RESULT_CODE_KEY_TYPE.DEFAULT]: '该错误码未定义，错误码为：'
};

/**
 * 获取业务响应错误信息
 * @param {*} code 网关状态码
 * @param {*} other 自定义响应数据
 * @returns 返回的错误报错信息
 */
function getBusinessErroMessage(code: RESULT_CODE_KEY_TYPE, other: string) {
  return RESULT_CODE[code] || other || `${RESULT_CODE[RESULT_CODE_KEY_TYPE.DEFAULT]}${code}`;
}

/**
 * 处理特殊需要统一全局处理的状态码以及逻辑
 */
const dealBusinessError = {
  1124: clearLogin, // token过期
  1125: notLogin, // 未登陆
  0: () => {}
};

export { dealBusinessError, getBusinessErroMessage };
