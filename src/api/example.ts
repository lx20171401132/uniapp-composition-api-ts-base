import { UPLOAD, GET } from '@/libs/index';

/**
 * 请求示例
 */

/**
 * 接口返回的数据格式
 */
interface ExampleReponseData {
  /**
   * 示例名称
   */
  name: string;
}

/**
 * 获取示例数据
 */
export function getExampleData(): Promise<ExampleReponseData> {
  return Promise.resolve({ name: 'example' });
}

/**
 * 上传文件
 */
export function uploadImage(filePath: string) {
  return UPLOAD({
    url: '/uploadFile',
    filePath: filePath,
    data: {
      test: 'aaa'
    },
    params: {
      admin: 'sss'
    },
    name: 'file'
  });
}

/**
 * 示例获取信息
 */
export function getExample() {
  return GET({
    url: '/getExample'
  });
}
