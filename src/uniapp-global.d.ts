/**
 * 本文件用于声明全局变量
 */

// // 声明 axios 第三方包
// declare module 'axios' {
//   import Axios from 'axios/index';
//   export default Axios;
// }

declare global {
  interface Window {
    name: string;
    App: Function;
  }
}
