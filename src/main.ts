import Vue from 'vue';
import App from './App.vue';
import VueCompositionAPI from '@vue/composition-api';
import { AppType } from '@/types/index';

// 使用composition-api方式
Vue.use(VueCompositionAPI);

Vue.config.productionTip = false;

// 注入mpType属性
(App as unknown as AppType).mpType = 'app';

const app = new Vue({
  ...App
});
app.$mount();
