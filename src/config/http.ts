/**
 * 是否开启debug的枚举
 */
export enum DEBUG_STATUS {
  OPEN = 1,
  CLOSE = 0
}

/**
 * 接口配置
 */
export interface HttpConfig {
  /**
   * 统一接口前缀
   */
  API_HOST: string;
  /**
   * 统一静态资源前缀
   */
  STATIC_HOST: string;
  /**
   * 统一静态资源前缀
   */
  OSS_HOST: string;
  /**
   * 统一上传域名前缀
   */
  UPLOAD_HOST: string;
  /**
   * 是否开启线上debug
   */
  debugState: DEBUG_STATUS;
}

/**
 * 接口配置
 */
export const HTTP_CONFIG: HttpConfig = {
  API_HOST: process.env.VUE_APP_BASE_API || '',
  STATIC_HOST: process.env.VUE_APP_BASE_STATIC_HOST || '',
  OSS_HOST: process.env.VUE_APP_BASE_OSS_HOST || '',
  UPLOAD_HOST: process.env.VUE_APP_BASE_UPLOAD_HOST || '',
  debugState: DEBUG_STATUS.CLOSE
};
