# 上贤软件工作室的 uniapp 的项目模版

## 项目简介

这是一个 uniapp 的项目模版，开箱即用，集成了一些常用配置，具体可以阅读以下内容。

### 项目技术栈

#### 主要技术项

    vue2
    vuex
    vuex-persistedstate
    vuex-module-decorators
    compositon-api
    uniapp
    typescript
    husky
    sass // 原为 node-sass，现替换为 dart-sass 方式，方便安装，有需要可以自行替换
    sass-loader
    mock-server-local
    whistle

## 环境准备

### 工具要求

- node
- 微信开发者工具
- whistle
  mock 和代理部分需要用到 whistle，需要提前安装：

```bash
npm i whistle -g
```

<span style="color: red">特别注意：</span>
<span style="color: green">如需要代理 https，则需要配置安装对应的证书，并正确信任，才能使用</span>

注意特别是 window 下，https 证书需要安装装在：
下载证书后，双击证书，根据指引安装证书。证书安装过程，要确保证书存储到受信任的根证书颁发机构下。

其他平台的证书的安装教程在：http://wproxy.org/whistle/webui/https.html

whistle 在谷歌浏览器上代理，还需要配合 SwitchyOmega 插件使用，对应插件包已经集成在项目根目录的 docs 文件夹下，
插件简易使用教程：
需要新增对应的情景模式，你可以起名为 whistle，代理协议为 http，代理服务器：127.0.0.1，代理端口为 7799（这个是和你启动 whistle 时候用的端口一样的）
看图：
<img style="width:400px;" src="./docs/插件设置示例.png" />

为什么是 7799 端口：

<img style="width:400px;" src="./docs/为什么是7799端口.png" />

#### git 提交规范以及分支管理规范参考

    地址：https://zhuanlan.zhihu.com/p/131332462

#### vscode 插件

    local history => 用于本地开发文件的历史记录，类似webstorm的功能
    git graph
    git history
    elsint
    vetur

#### 项目启动

```bash
npm i yarn -g
yarn
yarn serve
```

##### mock 启动

```bash
yarn mock
yarn w2
```

#### mock 能力集成

依赖能力：
mock-server-local
whistle
相关使用文档可以看案例或者官方文档查询
这个 mock 模式非常好用，还支持开发环境的代理以及小程序线上测试，推荐使用

```
"w2": "w2 stop && w2 start -p 7799 && w2 add --force .whistle.js", // whstle启动
"w2:stop": "w2 stop",
"w2:mock": "w2 stop && w2 start && w2 add --force .whistle.js --mockPort 7777",
"mock": "mock -p 7777 -d ./mock", // mock启动
```

#### 当前项目目录结构：

    根路径：
        .env.development // 开发编译配置
        .env.production // 生产环境编译配置
        .prettierrc // prettier-vscode插件的配置文件
        .prettierignore // prettier依赖的忽略格式化的配置文件
        prettier.config.js // prettier依赖的配置文件
        .eslintrc.js // eslint的配置文件
        .stylelintignore // stylelint依赖的忽略格式化的配置文件
        stylelint.config.js // stylelint依赖的配置文件
        lint-staged.config.js // lint-staged的配置文件
    mock：mock模拟数据
    src路径：
    api：存放通用的业务请求数据接口封装
    assets：资源存放处
    components：存放通用组件、业务组件
    config：项目配置（采用模块化）
    datas：存放静态json数据
    filters：存放全局的filters
    libs：第三方插件依赖库
        http：请求封装库
        message：弹窗显示库
    pakages: 分包页面
        user：示例
            每个分包都可以拥有自己的pages以及static
    pages：主包主页面
    static：静态资源存放
        icons：全局图标存放库
    store：全局状态库
        local: 持久化层
        modules: 分模块状态层
        index.ts: 入口文件
    styles：全局样式库
    utils：工具类
        使用小驼峰命名方式
    validator：通用数据校验库
    App.vue: 页面入口
    main.ts：逻辑入口

#### api 请求底层切换为 axios

    写了适配器进行更换普通请求等底层，upload仍使用原有版本
    路径：http/libs/axios
    目录解释：
        code: 通用错误响应封装
        base：基础请求包装
        fit-uniapp: 底层适配封装
        request-interceptor: 请求拦截器
        response-interceptor：响应拦截器

#### 问题记录

    当你使用多层绝对定位/其他定位时，记住，统一级别的都是根据祖元素进行diff你的z-index的
    动态列表和动态卡片都有封装一定的接口逻辑，我理解这个地方可以重构在页面组件触发对应的修改外部数据的组件

**1.项目内 node-sass 替换为 dart-sass，增加下载速度，需要可以自行替换回去**

<span style="color: red">注意：</span>dart-sass 不支持 sass 的/deep/的写法，需要更换成：::v-deep

**2.使用加载的 jest，微信小程序相关的命名空间的内容，eslint 报为定义问题**
只需要在 eslint 配置中加入下面代码即可：

```javascript
  env: {
    browser: true,
    node: true,
    es6: true,
    'jest/globals': true,
    jest: true,
    'weixin-app': true
  },
```

#### 项目代码测试搭建

```bash
yarn add -D @types/jest @vue/cli-plugin-unit-jest jest ts-jest
并创建：
jest.config.js文件编写配置
```

#### 自动格式化与配置

prettier // 自动格式化代码，用于统一代码格式
eslint // 代码规范检测
husky // 用于监听 git commit
lint-staged // 用于检测代码

##### 遇到的坑

husky 的新老版本配置不一致导致无法生效，网上大都是老版本的配置方式

老版本：安装 ——> 配置 4.x 版本

新版本：安装 ——> 启用 ——> 生成 .husky 相关配置 7.x

老版本配置需要在 package.json 加入以下配置：

```
{
  "husky": {
    "hooks": {
      "pre-commit": "lint-staged"
    }
  },
  "lint-staged": {
    "src/**/*.js": "prettier --write --ignore-unknown"
  }
}
```

husky 新版本（7.x）配置:

```
1.npm install husky lint-staged prettier --save-dev
2.npx husky install【启用】
  "prepare": "husky install"  【install后自动启用，这个操作一定要做，否则下次新拉代码还需要手动启用】
3.npx husky add .husky/pre-commit "npm run lint-staged"  【创建一个hook，执行完根目录回生成一个.husky目录】
继续在 scripts 里面添加　　"lint": "eslint --ext .js,.jsx,.ts,.tsx src", "lint-staged": "lint-staged"
package.json  里面添加 　   "lint-staged": { "**/*.{js,jsx,ts,tsx}": "npm run lint", "**/*.{js,jsx,tsx,ts,less,md}": [ "prettier --write" ] }
```

最后会在 package.json 文件加入下面的 script：

```
"lint": "eslint ./src/**/*.{js,jsx,vue,ts,tsx} --fix",
"prepare": "husky install",
"add:husky-config": "husky add .husky/pre-commit 'npm run lint-staged'",
"lint-staged": "lint-staged",
```

在 package.json 加入：

```
  "lint-staged": {
    "src/**/*.{jsx,txs,ts,js,vue}": [
      "prettier --config .prettierrc.js --write",
      "eslint --fix",
      "git add"
    ]
  },
```
